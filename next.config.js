/** @type {import('next').NextConfig} */
const nextConfig = {
  // We start firing off follow-accepted requests on render,
  // so we don't want that to run twice, even locally:
  reactStrictMode: false,
};

module.exports = nextConfig;
