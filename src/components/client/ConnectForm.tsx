"use client";

import { login } from "@inrupt/solid-client-authn-browser";
import { FormEventHandler, useState } from "react";
import { PiInfoBold } from "react-icons/pi";
import { BiLoaderCircle } from "react-icons/bi";
import { Button } from "./Button";
import { TextField } from "./TextField";

export const ConnectForm = () => {
  const [issuerInput, setIssuerInput] = useState("https://solidcommunity.net");
  const [isConnecting, setIsConnecting] = useState(false);

  const onSubmit: FormEventHandler = (event) => {
    event.preventDefault();

    login({ oidcIssuer: issuerInput, clientName: "Martin" });
    setIsConnecting(true);
  };

  return (
    <div className="flex flex-col items-center justify-between gap-24 p-24">
      <div className="max-w-xl mx-auto flex gap-2 p-2 border-4 border-black rounded-lg">
        <span className="flex-shrink-0">
          <PiInfoBold className="w-6 h-6" />
        </span>
        <p>
          <b>Good to know:</b> Martin is currently a proof-of-concept. Posts
          written using this version might not be supported in future versions.
        </p>
      </div>
      <form
        onSubmit={onSubmit}
        className="flex flex-col gap-4 items-stretch w-full max-w-xl "
      >
        <TextField
          label="Connect Pod:"
          type="url"
          value={issuerInput}
          onChange={(value) => setIssuerInput(value)}
          autoFocus
        />
        <Button type="submit" isProcessing={isConnecting}>
          Connect
        </Button>
      </form>
    </div>
  );
};
