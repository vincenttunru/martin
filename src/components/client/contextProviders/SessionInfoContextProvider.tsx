"use client";

import { ReactNode, useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import {
  EVENTS,
  getDefaultSession,
  handleIncomingRedirect,
  onLogout,
  onSessionRestore,
} from "@inrupt/solid-client-authn-browser";
import { SessionInfo, SessionInfoContext } from "../../../contexts/sessionInfo";

export const SessionInfoContextProvider = (props: { children: ReactNode }) => {
  const [sessionInfo, setSessionInfo] = useState<
    SessionInfo | null | undefined
  >(null);
  const router = useRouter();

  useEffect(() => {
    setSessionInfo(undefined);
    handleIncomingRedirect().then((info) => {
      if (info && info.isLoggedIn) {
        setSessionInfo(info as SessionInfo);
      } else {
        setSessionInfo(null);
      }
    });
    const defaultSession = getDefaultSession();
    const logoutListener = () => setSessionInfo(null);
    const sessionRestoreListener = (currentUrl: string) => {
      router.replace(currentUrl);
    };
    onLogout(logoutListener);
    onSessionRestore(sessionRestoreListener);

    return () => {
      defaultSession.removeListener(EVENTS.LOGOUT, logoutListener);
      defaultSession.removeListener(
        EVENTS.SESSION_RESTORED,
        sessionRestoreListener,
      );
    };
  }, [router]);

  return (
    <SessionInfoContext.Provider value={sessionInfo}>
      {props.children}
    </SessionInfoContext.Provider>
  );
};
