import { HTMLAttributes } from "react";

export const Time = ({
  time,
  ...otherProps
}: { time: Date } & HTMLAttributes<HTMLTimeElement>) => {
  const now = Date.now();
  const timestamp = time.getTime();
  if (
    timestamp > now - 2 * 24 * 60 * 60 * 1000 &&
    typeof Intl.RelativeTimeFormat !== "undefined"
  ) {
    const formatter = new Intl.RelativeTimeFormat("en-GB", {
      style: "long",
      numeric: "auto",
    });
    const minutesAgo = (timestamp - now) / (1000 * 60);
    let relativeTime = formatter.format(Math.round(minutesAgo), "minute");
    if (minutesAgo < -60) {
      relativeTime = formatter.format(Math.round(minutesAgo / 60), "hour");
    }
    if (minutesAgo < -60 * 24) {
      relativeTime = formatter.format(
        Math.round(minutesAgo / (60 * 24)),
        "day",
      );
    }
    return (
      <time
        dateTime={time.toISOString()}
        {...otherProps}
        title={new Intl.DateTimeFormat("en-GB", {
          dateStyle: "long",
          timeStyle: "medium",
        }).format(time)}
      >
        {relativeTime}
      </time>
    );
  }

  return (
    <time
      dateTime={time.toISOString()}
      {...otherProps}
      title={new Intl.DateTimeFormat("en-GB", {
        dateStyle: "long",
        timeStyle: "medium",
      }).format(time)}
    >
      {new Intl.DateTimeFormat("en-GB", { dateStyle: "long" }).format(time)}
    </time>
  );
};
