import { AriaProgressBarProps, useProgressBar } from "react-aria";

export const ProgressBar = (
  props: AriaProgressBarProps & Required<Pick<AriaProgressBarProps, "value">>,
) => {
  const { progressBarProps, labelProps } = useProgressBar(props);

  const minValue = props.minValue ?? 0;
  const maxValue = props.maxValue ?? 100;
  const percentage = (props.value - minValue) / (maxValue - minValue);
  const barWidth = `${Math.round(percentage * 100)}%`;

  return (
    <div {...progressBarProps} className="w-80 flex flex-col gap-2">
      <div className="flex content-between">
        {props.label && (
          <span {...labelProps} className="text-base text-center w-full">
            {props.label}
          </span>
        )}
      </div>
      <div className="h-10 bg-amber-300 rounded">
        <div
          style={{ width: barWidth }}
          className="h-10 bg-amber-500 rounded transition-all duration-500"
        />
      </div>
    </div>
  );
};
