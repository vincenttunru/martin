import { useState } from "react";
import { BiLoaderCircle } from "react-icons/bi";
import {
  UrlString,
  createContainerAt,
  createSolidDataset,
  getThing,
  getUrl,
  overwriteFile,
  saveSolidDatasetAt,
} from "@inrupt/solid-client";
import { setPublicAccess } from "@inrupt/solid-client/universal";
import { fetch } from "@inrupt/solid-client-authn-browser";
import { SessionInfo } from "../../contexts/sessionInfo";
import { useRoot } from "../../hooks/data/root";
import { usePrivateKey, usePublicKey } from "../../hooks/data/keys";
import { useActor } from "../../hooks/data/actor";
import { useFollowerIndex } from "../../hooks/data/followerIndex";
import { isLoadedFileData } from "../../hooks/data/file";
import {
  CachedDataset,
  isLoadedDataset,
  useDataset,
} from "../../hooks/data/dataset";
import { ActorCreator } from "./view/ActorCreator";
import {
  getActorUrl,
  getFollowerIndexUrl,
  getInboxUrl,
  getOutboxUrl,
  getPrivateKeyUrl,
  getPublicKeyUrl,
  getWebfingerUrl,
} from "../../functions/getUrl";
import {
  exportPrivateKey,
  exportPublicKey,
  generateKeyPair,
} from "../../functions/key";
import { Poster } from "./view/Poster";
import { useOutbox } from "../../hooks/data/outbox";
import { PostHistory } from "./view/PostHistory";
import { solid, space } from "rdf-namespaces";
import { ProgressBar } from "./ProgressBar";

export const ConnectedUi = (props: { sessionInfo: SessionInfo }) => {
  const [initialisationProgress, setInitialisationProgress] = useState(0);
  const profileDataset = useDataset(props.sessionInfo.webId);
  const webIdRoot = getStorageRoot(profileDataset, props.sessionInfo.webId);
  const traversedRoot = useRoot(webIdRoot ? null : props.sessionInfo.webId);
  const root = webIdRoot ?? traversedRoot;
  const actorResponse = useActor(root ?? null, { shouldRetryOnError: false });
  const privateKeyResponse = usePrivateKey(root ?? null, {
    shouldRetryOnError: false,
  });
  const publicKeyResponse = usePublicKey(root ?? null, {
    shouldRetryOnError: false,
  });
  const followerIndexResponse = useFollowerIndex(root ?? null, {
    shouldRetryOnError: false,
  });
  const outboxResponse = useOutbox(root ?? null, { shouldRetryOnError: false });

  const responses = [
    actorResponse,
    privateKeyResponse,
    publicKeyResponse,
    followerIndexResponse,
    outboxResponse,
  ];

  if (root === null) {
    return (
      <div className="flex flex-col items-center justify-between p-24">
        There appears to be a problem with your Pod.
      </div>
    );
  }

  const loadingResponses = responses.filter(
    (response) => response.isLoading && !response.error,
  );
  if (typeof root === "undefined" || loadingResponses.length !== 0) {
    return (
      <div className="mx-auto my-14 flex flex-col gap-5 max-w-3xl items-center">
        <ProgressBar
          label={
            <span className="flex gap-2 items-center justify-center">
              Checking Pod <BiLoaderCircle className="animate-spin" />
            </span>
          }
          maxValue={responses.length}
          value={
            typeof root === "undefined"
              ? 0
              : responses.length - loadingResponses.length
          }
        />
      </div>
    );
  }

  const rootUrl = new URL(root);
  if (rootUrl.pathname !== "/") {
    return (
      <div className="p-24 max-w-3xl mx-auto">
        Unfortunately, Martin will only work if your Pod is at the root of a
        domain{" "}
        <span className="whitespace-nowrap">
          (e.g.{" "}
          <i className="font-mono">
            https://example.solidcommunity.net
            <span className="font-bold text-blue-600">/</span>
          </i>
          ).
        </span>{" "}
        Yours is at:
        <br />
        <span className="font-mono whitespace-nowrap block p-5">
          {rootUrl.origin}
          <output className="text-blue-600 font-bold">/</output>
          <output className="text-red-700 font-bold">
            {rootUrl.pathname.substring(1)}
          </output>
        </span>
        .
      </div>
    );
  }

  if (
    !isLoadedDataset(actorResponse) ||
    !isLoadedFileData(privateKeyResponse) ||
    !isLoadedFileData(publicKeyResponse) ||
    !isLoadedDataset(followerIndexResponse)
  ) {
    if (initialisationProgress > 0) {
      return (
        <div className="mx-auto my-14 flex flex-col gap-5 max-w-3xl items-center">
          <ProgressBar
            label={
              <span className="flex gap-2 items-center justify-center">
                Setting up your Pod <BiLoaderCircle className="animate-spin" />
              </span>
            }
            maxValue={100}
            value={initialisationProgress}
          />
        </div>
      );
    }

    return (
      <ActorCreator
        hostname={new URL(root).hostname}
        onSet={async (username) => {
          const actorUrl = getActorUrl(root);
          const webfingerUrl = getWebfingerUrl(root);
          const inboxUrl = getInboxUrl(root);
          const outboxUrl = getOutboxUrl(root);
          const publicKeyThingUrl = getActorUrl(root);
          publicKeyThingUrl.hash = "#main-key";
          const privateKeyUrl = getPrivateKeyUrl(root);
          const publicKeyUrl = getPublicKeyUrl(root);
          const followerIndexUrl = getFollowerIndexUrl(root);

          const keyPair = await generateKeyPair();
          setInitialisationProgress(5);
          const exportedPrivateKey = await exportPrivateKey(keyPair.privateKey);
          setInitialisationProgress(10);
          const exportedPublicKey = await exportPublicKey(keyPair.publicKey);
          setInitialisationProgress(15);

          const actorJsonLd = {
            "@context": [
              "https://www.w3.org/ns/activitystreams",
              "https://w3id.org/security/v1",
            ],

            id: actorUrl.href,
            type: "Person",
            preferredUsername: username,
            inbox: getInboxUrl(root).href,
            outbox: getOutboxUrl(root).href,

            publicKey: {
              id: publicKeyThingUrl.href,
              owner: actorUrl.href,
              publicKeyPem: exportedPublicKey,
            },
          };
          const webfingerJson = {
            subject: `acct:${username}@${webfingerUrl.hostname}`,
            links: [
              {
                rel: "self",
                type: "application/activity+json",
                href: actorUrl.href,
              },
            ],
          };

          await Promise.all([
            overwriteFile(
              actorUrl.href,
              new Blob([JSON.stringify(actorJsonLd, null, 4)]),
              { fetch: fetch, contentType: " application/ld+json" },
            ).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            overwriteFile(
              webfingerUrl.href,
              new Blob([JSON.stringify(webfingerJson, null, 4)]),
              { fetch: fetch, contentType: "application/json" },
            ).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            createContainerAt(inboxUrl.href, { fetch: fetch }).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            createContainerAt(outboxUrl.href, { fetch: fetch }).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            overwriteFile(privateKeyUrl.href, new Blob([exportedPrivateKey]), {
              fetch: fetch,
              contentType: "text/plain",
            }).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            overwriteFile(publicKeyUrl.href, new Blob([exportedPublicKey]), {
              fetch: fetch,
              contentType: "text/plain",
            }).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            saveSolidDatasetAt(followerIndexUrl.href, createSolidDataset(), {
              fetch: fetch,
            }).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
          ]);
          await Promise.all([
            setPublicAccess(
              actorUrl.href,
              { read: true },
              { fetch: fetch },
            ).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            setPublicAccess(
              webfingerUrl.href,
              { read: true },
              { fetch: fetch },
            ).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            setPublicAccess(
              inboxUrl.href,
              { append: true },
              { fetch: fetch },
            ).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
            setPublicAccess(
              publicKeyUrl.href,
              { read: true },
              { fetch: fetch },
            ).then(() =>
              setInitialisationProgress((oldProgress) => oldProgress + 5),
            ),
          ]);
          responses.forEach((response) =>
            response
              .mutate()
              .then(() =>
                setInitialisationProgress((oldProgress) => oldProgress + 5),
              ),
          );
        }}
      />
    );
  }

  return (
    <>
      <Poster
        actor={actorResponse}
        privateKey={privateKeyResponse}
        publicKey={publicKeyResponse}
        followerIndex={followerIndexResponse}
        root={root}
        onPost={() => outboxResponse.mutate()}
      />
      {isLoadedDataset(outboxResponse) && (
        <PostHistory outbox={outboxResponse} />
      )}
    </>
  );
};

const getStorageRoot = (
  dataset: CachedDataset,
  webId: UrlString,
): UrlString | null => {
  if (!isLoadedDataset(dataset)) {
    return null;
  }

  const profileThing = getThing(dataset.data, webId);
  if (!profileThing) {
    return null;
  }
  const storage = getUrl(profileThing, space.storage);
  if (storage) {
    return storage;
  }

  return getUrl(profileThing, solid.account);
};
