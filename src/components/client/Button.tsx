"use client";

import { useRef } from "react";
import { AriaButtonProps, useButton } from "react-aria";
import { BiLoaderCircle } from "react-icons/bi";

export const Button = (props: AriaButtonProps & { isProcessing?: boolean }) => {
  const ref = useRef<HTMLButtonElement>(null);
  const { buttonProps } = useButton(props, ref);

  return (
    <button
      {...buttonProps}
      ref={ref}
      className={`bg-black text-white p-4 font-bold rounded-lg focus:outline focus:outline-amber-600 ${
        props.isDisabled ? "opacity-10 cursor-not-allowed" : ""
      }`}
    >
      {props.isProcessing ? (
        <BiLoaderCircle className="animate-spin inline-block" />
      ) : (
        props.children
      )}
    </button>
  );
};
