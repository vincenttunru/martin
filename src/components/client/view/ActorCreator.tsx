import { FormEventHandler, ReactNode, useState } from "react";
import { TextField } from "../TextField";
import { BiLoaderCircle } from "react-icons/bi";
import { Button } from "../Button";

export type Props = {
  hostname: string;
  onSet: (username: string) => Promise<void>;
};

export const ActorCreator = (props: Props) => {
  const [usernameInput, setUsernameInput] = useState("");
  const [isSaving, setIsSaving] = useState(false);

  let validationError: ReactNode;
  if (usernameInput.length > 0) {
    if (
      usernameInput.includes(" ") ||
      usernameInput.includes("\t") ||
      usernameInput.includes("\n")
    ) {
      validationError = "You handle cannot contain spaces.";
    } else if (usernameInput.includes("@")) {
      validationError = "Your handle cannot include an @ symbol.";
    } else if (hasInvalidCharacters(usernameInput)) {
      validationError = "Your handle contains invalid characters";
    }
  }

  const onSubmit: FormEventHandler = (event) => {
    event.preventDefault();

    if (validationError) {
      return;
    }

    setIsSaving(true);
    props.onSet(usernameInput);
  };

  return (
    <div className="flex flex-col items-center justify-between p-24">
      <form
        onSubmit={onSubmit}
        className="flex flex-col gap-4 items-stretch w-full max-w-xl "
      >
        <TextField
          label="How would you like to be known?"
          value={usernameInput}
          onChange={(value) => setUsernameInput(value)}
          description={
            <>
              Your handle will be{" "}
              <span className="font-mono">
                <output className="text-blue-600 font-bold">
                  @{usernameInput || "…"}
                </output>
                @{props.hostname}
              </span>
            </>
          }
          placeholder="e.g. `iamnils`"
          isRequired={true}
          errorMessage={validationError}
        />
        <Button
          type="submit"
          isDisabled={usernameInput.length === 0 || !!validationError}
          isProcessing={isSaving}
        >
          Set
        </Button>
      </form>
    </div>
  );
};

function hasInvalidCharacters(input: string): boolean {
  try {
    new URL(`https://${input}.example.com`);
  } catch (e) {
    return true;
  }
  return false;
}
