import {
  UrlString,
  getDatetime,
  getStringNoLocale,
  getThing,
  getUrl,
} from "@inrupt/solid-client";
import { isLoadedDataset, useDataset } from "../../../hooks/data/dataset";
import { rdf } from "rdf-namespaces";
import { Time } from "../Time";

export type Props = {
  postUrl: UrlString;
};

export const Post = (props: Props) => {
  const dataset = useDataset(props.postUrl);

  if (!isLoadedDataset(dataset)) {
    return null;
  }

  const datasetThing = getThing(dataset.data, props.postUrl);
  if (
    !datasetThing ||
    getUrl(datasetThing, rdf.type) !==
      "https://www.w3.org/ns/activitystreams#Create"
  ) {
    return null;
  }

  const postUrl = getUrl(
    datasetThing,
    "https://www.w3.org/ns/activitystreams#object",
  );
  if (!postUrl) {
    return null;
  }

  const postThing = getThing(dataset.data, postUrl);

  if (
    !postThing ||
    getUrl(postThing, rdf.type) !== "https://www.w3.org/ns/activitystreams#Note"
  ) {
    return null;
  }

  const postDate = getDatetime(
    postThing,
    "https://www.w3.org/ns/activitystreams#published",
  );
  const postContent = getStringNoLocale(
    postThing,
    "https://www.w3.org/ns/activitystreams#content",
  );

  if (!postDate || !postContent) {
    return null;
  }

  return (
    <section className="flex flex-col gap-4 items-stretch w-full max-w-xl ">
      <div className="w-full">{postContent}</div>
      <Time time={postDate} className="text-base text-gray-700 text-end" />
    </section>
  );
};
