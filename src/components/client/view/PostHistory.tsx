import {
  getContainedResourceUrlAll,
  getDatetime,
  getThing,
} from "@inrupt/solid-client";
import { LoadedCachedDataset } from "../../../hooks/data/dataset";
import { Post } from "./Post";
import { dct } from "rdf-namespaces";

export type Props = {
  outbox: LoadedCachedDataset;
};

export const PostHistory = (props: Props) => {
  const outboxUrls = getContainedResourceUrlAll(props.outbox.data);

  const sortedOutboxUrls = sort(outboxUrls, (urlA, urlB) => {
    const outboxAThing = getThing(props.outbox.data, urlA);
    const outboxBThing = getThing(props.outbox.data, urlB);
    if (!outboxAThing || !outboxBThing) {
      return 0;
    }

    const modifiedA = getDatetime(outboxAThing, dct.modified);
    const modifiedB = getDatetime(outboxBThing, dct.modified);
    if (!modifiedA || !modifiedB) {
      return 0;
    }

    return modifiedB.getTime() - modifiedA.getTime();
  });

  return (
    <>
      <div className="flex flex-col items-center justify-between gap-24 p-24">
        <div className="flex flex-col items-center gap-24 w-full max-w-xl">
          {sortedOutboxUrls.map((url) => (
            <Post key={url} postUrl={url} />
          ))}
        </div>
      </div>
    </>
  );
};

function sort<T>(arr: T[], compareFn: Parameters<typeof arr.toSorted>[0]): T[] {
  if (typeof arr.toSorted === "function") {
    return arr.toSorted(compareFn);
  }

  const arrCopy = [...arr];
  arrCopy.sort(compareFn);
  return arrCopy;
}
