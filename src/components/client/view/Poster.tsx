import { PiInfoBold } from "react-icons/pi";
import { getActorData } from "../../../functions/data/actor";
import { LoadedCachedDataset } from "../../../hooks/data/dataset";
import { LoadedCachedFileData } from "../../../hooks/data/file";
import { getFollowerList } from "../../../functions/data/followers";
import {
  Data as FollowRequestData,
  useFollowRequests,
} from "../../../hooks/data/followRequests";
import { FormEventHandler, useCallback, useMemo, useState } from "react";
import {
  ThingPersisted,
  UrlString,
  getSourceUrl,
  setThing,
} from "@inrupt/solid-client";
import { TextField } from "../TextField";
import { TextArea } from "../TextArea";
import { Button } from "../Button";
import { createPost } from "../../../functions/createPost";

export type Props = {
  root: UrlString;
  privateKey: LoadedCachedFileData;
  publicKey: LoadedCachedFileData;
  actor: LoadedCachedDataset;
  followerIndex: LoadedCachedDataset;
  onPost: () => void;
};

export const Poster = (props: Props) => {
  const actorData = useMemo(
    () => getActorData(props.actor.data),
    [props.actor.data],
  );
  const followerUrlList = getFollowerList(props.followerIndex.data);
  const onApprovedFollowRequests = useCallback(
    (approvedFollowRequests: ThingPersisted[]) => {
      if (approvedFollowRequests.length === 0) {
        return;
      }

      const updatedFollowerDataset = approvedFollowRequests.reduce(
        (followerIndex, approvedFollowRequest) => {
          return setThing(followerIndex, approvedFollowRequest);
        },
        props.followerIndex.data,
      );
      props.followerIndex.save(updatedFollowerDataset);
    },
    [props.followerIndex],
  );
  useFollowRequests(
    props.root,
    props.privateKey,
    actorData,
    onApprovedFollowRequests,
  );

  const [postInput, setPostInput] = useState("");
  const [isPosting, setIsPosting] = useState(false);

  const onSubmit: FormEventHandler = async (event) => {
    event.preventDefault();

    setIsPosting(true);
    const notifedFollowerList = await createPost(
      props.root,
      props.privateKey,
      actorData,
      followerUrlList,
      postInput,
    );
    // TODO: Save posts in a specific outbox folder, and render those:
    console.log("Posted and shared to:", notifedFollowerList);
    setPostInput("");
    setIsPosting(false);
    props.onPost();
  };

  return (
    <>
      <div className="flex flex-col items-center justify-between gap-24 p-24">
        <div className="flex items-center gap-2">
          <PiInfoBold className="w-8 h-8" />
          <h2 className="font-bold">
            Posting as {actorData.handle} to {followerUrlList.length} followers.
          </h2>
        </div>

        <form
          onSubmit={onSubmit}
          className="flex flex-col gap-4 items-stretch w-full max-w-xl "
        >
          <TextArea
            label="What's up?"
            value={postInput}
            onChange={(value) => setPostInput(value)}
            autoFocus
          />
          <Button
            type="submit"
            isDisabled={postInput.length === 0}
            isProcessing={isPosting}
          >
            Honk!
          </Button>
        </form>
      </div>
    </>
  );
};
