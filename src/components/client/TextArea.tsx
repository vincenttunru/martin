"use client";

import { useRef } from "react";
import { AriaTextFieldProps, useTextField } from "react-aria";

export const TextArea = (props: AriaTextFieldProps) => {
  const ref = useRef<HTMLTextAreaElement>(null);
  const { labelProps, inputProps, descriptionProps, errorMessageProps } =
    useTextField({ ...props, inputElementType: "textarea" }, ref);

  return (
    <div className="flex flex-col gap-4">
      <label {...labelProps}>{props.label}</label>
      <textarea
        {...inputProps}
        ref={ref}
        className={`p-4 h-40 rounded-lg ${
          props.errorMessage
            ? "outline outline-red-700"
            : "focus:outline focus:outline-amber-600"
        }`}
      />
      {props.description && (
        <div {...descriptionProps} className="text-base text-zinc-800">
          {props.description}
        </div>
      )}
      {props.errorMessage && (
        <div {...errorMessageProps} className="text-base text-red-700">
          {props.errorMessage}
        </div>
      )}
    </div>
  );
};
