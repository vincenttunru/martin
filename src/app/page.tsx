"use client";

import { SessionInfoContextProvider } from "../components/client/contextProviders/SessionInfoContextProvider";
import { View } from "./View";

export default function Home() {
  return (
    <SessionInfoContextProvider>
      <View />
    </SessionInfoContextProvider>
  );
}
