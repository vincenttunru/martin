import "./globals.css";
import type { Metadata } from "next";
import { Lilita_One, Sarala } from "next/font/google";

const titleFont = Lilita_One({ weight: "400", subsets: ["latin"] });
const bodyFont = Sarala({ weight: "400", subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Martin",
  description: "Honk!",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className="bg-amber-200 selection:bg-amber-600">
        <div className="flex flex-col h-full">
          <header className="flex-shrink-0 text-center p-10">
            <h1 className={`${titleFont.className} text-4xl`}>Martin</h1>
          </header>
          <div className={`flex-grow h-full ${bodyFont.className} text-xl`}>
            {children}
          </div>
        </div>
      </body>
    </html>
  );
}
