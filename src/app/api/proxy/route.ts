import { NextRequest, NextResponse } from "next/server";

// Use the Edge runtime for this API route; it doesn't do anything special,
// so a free performance improvement with supporting deployers is nice:
// https://nextjs.org/docs/app/building-your-application/rendering/edge-and-nodejs-runtimes#segment-runtime-option
export const runtime = "edge";

export type ProxyRequestBody = {
  target: string;
  method: RequestInit["method"];
  body: RequestInit["body"];
  headers: RequestInit["headers"];
};

export async function POST(request: NextRequest) {
  const requestBody: ProxyRequestBody = await request.json();

  const proxiedResponse = await fetch(requestBody.target, {
    method: requestBody.method,
    headers: requestBody.headers,
    body: requestBody.body,
  });

  return new NextResponse(await proxiedResponse.text(), {
    status: proxiedResponse.status,
    statusText: proxiedResponse.statusText,
  });
}
