import { ConnectForm } from "../components/client/ConnectForm";
import { ConnectedUi } from "../components/client/ConnectedUi";
import { useSessionInfo } from "../hooks/sessionInfo";

export const View = () => {
  const sessionInfo = useSessionInfo();

  return (
    <main className="h-full">
      {!sessionInfo ? (
        <ConnectForm />
      ) : (
        <ConnectedUi sessionInfo={sessionInfo} />
      )}
    </main>
  );
};
