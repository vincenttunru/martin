import { UrlString } from "@inrupt/solid-client";
import { useFile } from "./file";
import { getPrivateKeyUrl, getPublicKeyUrl } from "../../functions/getUrl";
import { SWRConfiguration } from "swr";

export const usePrivateKey = (
  root: UrlString | null,
  options?: SWRConfiguration,
) => {
  const fileUrl = root ? getPrivateKeyUrl(root).href : null;
  const response = useFile(fileUrl, options);

  return response;
};

export const usePublicKey = (
  root: UrlString | null,
  options?: SWRConfiguration,
) => {
  const fileUrl = root ? getPublicKeyUrl(root).href : null;
  const response = useFile(fileUrl, options);

  return response;
};
