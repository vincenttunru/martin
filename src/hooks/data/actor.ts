import { UrlString } from "@inrupt/solid-client";
import { useDataset } from "./dataset";
import { getActorUrl } from "../../functions/getUrl";
import { SWRConfiguration } from "swr";

export const useActor = (
  root: UrlString | null,
  options?: SWRConfiguration,
) => {
  const fileUrl = root ? getActorUrl(root).href : null;
  const response = useDataset(fileUrl, options);

  return response;
};
