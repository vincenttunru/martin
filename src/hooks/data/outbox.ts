import { UrlString } from "@inrupt/solid-client";
import { useDataset } from "./dataset";
import { getOutboxUrl } from "../../functions/getUrl";
import { SWRConfiguration } from "swr";

export const useOutbox = (
  root: UrlString | null,
  options?: SWRConfiguration,
) => {
  const fileUrl = root ? getOutboxUrl(root).href : null;
  const response = useDataset(fileUrl, options);

  return response;
};
