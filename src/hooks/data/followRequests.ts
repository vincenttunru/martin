import {
  ThingPersisted,
  UrlString,
  asUrl,
  deleteSolidDataset,
  getContainedResourceUrlAll,
  getJsonLdParser,
  getThingAll,
  getUrl,
  overwriteFile,
  responseToSolidDataset,
} from "@inrupt/solid-client";
import { useDataset } from "./dataset";
import { useEffect, useState } from "react";
import { fetch } from "@inrupt/solid-client-authn-browser";
import { rdf } from "rdf-namespaces";
import { fetchActor, parseActor } from "../../functions/fetchActor";
import { v4 } from "uuid";
import { postSigned } from "../../functions/postSigned";
import { LoadedCachedFileData } from "./file";
import { isNotNull } from "../../functions/isNotNull";
import { getActorUrl, getInboxUrl, getOutboxUrl } from "../../functions/getUrl";
import { ActorData } from "../../functions/data/actor";

export type Data = {
  inboxUrl: UrlString;
  outboxUrl: UrlString;
  actorUrl: UrlString;
  actorData: ActorData;
  privateKey: LoadedCachedFileData;
};

export const useFollowRequests = (
  root: UrlString,
  privateKey: LoadedCachedFileData,
  actorData: ActorData,
  onDone: (approvedFollowRequests: ThingPersisted[]) => void,
) => {
  const [hasLoaded, setHasLoaded] = useState(false);
  const inboxContainer = useDataset(getInboxUrl(root).href, {
    isPaused: () => hasLoaded,
  });

  useEffect(() => {
    if (!inboxContainer.data || hasLoaded) {
      return;
    }
    setHasLoaded(true);
    const data: Data = {
      inboxUrl: getInboxUrl(root).href,
      outboxUrl: getOutboxUrl(root).href,
      actorUrl: getActorUrl(root).href,
      actorData: actorData,
      privateKey: privateKey,
    };

    const inboxDatasetUrlList = getContainedResourceUrlAll(inboxContainer.data);
    try {
      Promise.all(
        inboxDatasetUrlList.map((inboxDatasetUrl) =>
          approveFollow(data, inboxDatasetUrl),
        ),
      ).then((followRequests) => {
        const approvedFollowRequests = followRequests.filter(isNotNull);
        onDone(approvedFollowRequests);
      });
    } catch (e) {
      // Could not approve followers; do nothing
      console.log("Encountered an error approving follow requests:", e);
    }
  }, [actorData, hasLoaded, inboxContainer.data, onDone, privateKey, root]);
};

async function approveFollow(
  data: Data,
  inboxDatasetUrl: UrlString,
): Promise<ThingPersisted | null> {
  const response = await fetch(inboxDatasetUrl);
  const inboxMessageDataset = await responseToSolidDataset(response, {
    parsers: {
      "application/ld+json": getJsonLdParser(),
      // Mastodon posts the follow requests without a Content-Type header,
      // and thus the Solid server serves it as application/octet-stream:
      "application/octet-stream": getJsonLdParser(),
    },
  });

  const inboxMessageThings = getThingAll(inboxMessageDataset);
  // TODO: Check that it's not the target of a thing of type "as:Undo"?
  const followThings: ThingPersisted[] = inboxMessageThings.filter(
    (thing) =>
      getUrl(thing, rdf.type) ===
      "https://www.w3.org/ns/activitystreams#Follow",
  );
  const followRequest = followThings[0];
  if (!followRequest) {
    return null;
  }

  const approvedFollowRequest = await sendApproval(data, followRequest);

  if (approvedFollowRequest !== null) {
    await deleteSolidDataset(inboxDatasetUrl, { fetch: fetch });
  }

  return approvedFollowRequest;
}

async function sendApproval(
  data: Data,
  followRequest: ThingPersisted,
): Promise<ThingPersisted | null> {
  const followerUrl = getUrl(
    followRequest,
    "https://www.w3.org/ns/activitystreams#actor",
  );
  if (followerUrl === null) {
    return null;
  }

  const actor = await fetchActor(followerUrl);
  if (actor === null) {
    return null;
  }

  const parsedActor = parseActor(actor);
  if (parsedActor === null) {
    return null;
  }

  const outboxMessageUrl = data.outboxUrl + v4();

  const approveJsonLd = {
    "@context": "https://www.w3.org/ns/activitystreams",

    id: outboxMessageUrl,
    type: "Accept",
    actor: data.actorUrl,

    object: {
      id: asUrl(followRequest),
      type: "Follow",
      published: new Date(Date.now()).toISOString(),
      attributedTo: followerUrl,
    },
  };
  try {
    const response = await postSigned(
      data.privateKey,
      data.actorData,
      parsedActor.inboxUrl,
      JSON.stringify(approveJsonLd),
    );
    if (!response.ok) {
      return null;
    }
    await overwriteFile(
      outboxMessageUrl,
      new Blob([JSON.stringify(approveJsonLd, null, 2)]),
      { fetch: fetch, contentType: "application/ld+json" },
    );
  } catch (e) {
    return null;
  }

  return followRequest;
}
