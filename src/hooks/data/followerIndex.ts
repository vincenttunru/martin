import { UrlString } from "@inrupt/solid-client";
import { useDataset } from "./dataset";
import { getFollowerIndexUrl } from "../../functions/getUrl";
import { SWRConfiguration } from "swr";

export const useFollowerIndex = (
  root: UrlString | null,
  options?: SWRConfiguration,
) => {
  const fileUrl = root ? getFollowerIndexUrl(root).href : null;
  const response = useDataset(fileUrl, options);

  return response;
};
