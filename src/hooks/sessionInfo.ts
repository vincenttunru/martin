import { useContext } from "react";
import { SessionInfoContext } from "../contexts/sessionInfo";

export function useSessionInfo() {
  const sessionInfo = useContext(SessionInfoContext);

  return sessionInfo;
}
