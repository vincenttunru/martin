import { getJsonLdParser, getSolidDataset } from "@inrupt/solid-client";
import { fetch } from "@inrupt/solid-client-authn-browser";

export const fetchJsonLdDataset: typeof getSolidDataset = (url, options) => {
  return getSolidDataset(url, {
    fetch: fetch,
    parsers: {
      "application/ld+json": getJsonLdParser(),
      "application/activity+json": getJsonLdParser(),
      ...options?.parsers,
    },
    ...options,
  });
};
