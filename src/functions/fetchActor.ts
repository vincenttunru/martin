import {
  ThingPersisted,
  UrlString,
  getStringNoLocale,
  getThing,
  getUrl,
} from "@inrupt/solid-client";
import { ldp } from "rdf-namespaces";
import { fetchJsonLdDataset } from "./fetchJsonLdDataset";

export async function fetchActor(
  actorUrl: UrlString,
): Promise<ThingPersisted | null> {
  const actorDoc = await fetchJsonLdDataset(actorUrl);
  const actorThing = getThing(actorDoc, actorUrl);
  return actorThing;
}

export type ParsedActor = {
  inboxUrl: UrlString;
  outboxUrl: UrlString;
  publicKeyUrl: UrlString;
  name?: string;
};

export function parseActor(actorThing: ThingPersisted): ParsedActor | null {
  const inboxUrl = getUrl(actorThing, ldp.inbox);
  const outboxUrl = getUrl(
    actorThing,
    "https://www.w3.org/ns/activitystreams#outbox",
  );
  const publicKeyUrl = getUrl(
    actorThing,
    "https://w3id.org/security#publicKey",
  );
  const name = getStringNoLocale(
    actorThing,
    "https://www.w3.org/ns/activitystreams#name",
  );

  if (inboxUrl === null || outboxUrl === null || publicKeyUrl === null) {
    return null;
  }

  return {
    inboxUrl,
    outboxUrl,
    publicKeyUrl,
    name: name ?? undefined,
  };
}
