import { UrlString } from "@inrupt/solid-client";

const mainContainer = "martin_beta";

export function getActorUrl(podRoot: UrlString): URL {
  return new URL(`/${mainContainer}/profile/actor`, podRoot);
}

export function getWebfingerUrl(podRoot: UrlString): URL {
  return new URL(`/.well-known/webfinger`, podRoot);
}

export function getInboxUrl(podRoot: UrlString): URL {
  return new URL(`/${mainContainer}/inbox/`, podRoot);
}

export function getOutboxUrl(podRoot: UrlString): URL {
  return new URL(`/${mainContainer}/outbox/`, podRoot);
}

export function getFollowerIndexUrl(podRoot: UrlString): URL {
  return new URL(`/${mainContainer}/followers`, podRoot);
}

export function getPrivateKeyUrl(podRoot: UrlString): URL {
  return new URL(`/${mainContainer}/keys/private.pem`, podRoot);
}

export function getPublicKeyUrl(podRoot: UrlString): URL {
  return new URL(`/${mainContainer}/keys/public.pem`, podRoot);
}
