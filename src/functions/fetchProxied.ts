import { ProxyRequestBody } from "../app/api/proxy/route";

/**
 * This function has the same API as `fetch`, but proxies the request through our backend.
 *
 * This is a workaround to be able to make cross-origin requests.
 */
export const fetchProxied = async (input: URL | string, init: RequestInit) => {
  const body: ProxyRequestBody = {
    target: input instanceof URL ? input.href : input,
    body: init.body,
    headers: Object.fromEntries(new Headers(init.headers).entries()),
    method: init.method,
  };
  return fetch("/api/proxy/", {
    method: "POST",
    body: JSON.stringify(body),
  });
};
