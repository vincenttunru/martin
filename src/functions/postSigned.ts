import { UrlString } from "@inrupt/solid-client";
import { fetchProxied } from "./fetchProxied";
import { digest } from "./digest";
import { importPrivateKey } from "./key";
import { LoadedCachedFileData } from "../hooks/data/file";
import { ActorData } from "./data/actor";

export async function postSigned(
  privateKey: LoadedCachedFileData,
  actorData: ActorData,
  target: UrlString,
  body: string,
): Promise<Response> {
  const dateString = new Date(Date.now()).toUTCString();
  const targetUrl = new URL(target);
  const digestHeader = `sha-256=${await digest(body)}`;
  const signatureString = `(request-target): post ${targetUrl.pathname}
host: ${targetUrl.host}
date: ${dateString}
digest: ${digestHeader}`;
  const signature = window.btoa(
    await sign(signatureString, await privateKey.data.blob.text()),
  );
  const signatureHeader = `keyId="${actorData.publicKeyUrl}",headers="(request-target) host date digest",signature="${signature}"`;
  const headers = new Headers({
    Host: targetUrl.host,
    Date: dateString,
    Digest: digestHeader,
    Signature: signatureHeader,
  });

  // Unfortunately Mastodon doesn't set the requisite Access-Control-Allow-Origin
  // headers that the browser checks before making a request, so we can't send
  // these requests client-side. We therefore proxy them through the server, but
  // since the signing is all done client-side, the proxy server doesn't actually
  // get access to the user's private data:
  return fetchProxied(target, {
    body: body,
    method: "POST",
    headers: headers,
  });
}

async function sign(data: string, keyString: string): Promise<string> {
  const encoded = new TextEncoder().encode(data);
  const key = await importPrivateKey(keyString);
  const signature = await crypto.subtle.sign(
    { name: "RSASSA-PKCS1-v1_5" },
    key,
    encoded,
  );
  const buffer = new Uint8Array(signature);
  const bufferArray = Array.from(buffer);
  const signedString = String.fromCharCode.apply(null, bufferArray);
  return signedString;
}
