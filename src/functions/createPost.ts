import { UrlString, overwriteFile } from "@inrupt/solid-client";
import { fetch } from "@inrupt/solid-client-authn-browser";
import { v4 } from "uuid";
import { fetchActor, parseActor } from "./fetchActor";
import { postSigned } from "./postSigned";
import { LoadedCachedFileData } from "../hooks/data/file";
import { getActorUrl, getOutboxUrl } from "./getUrl";
import { ActorData } from "./data/actor";

export async function createPost(
  root: UrlString,
  privateKey: LoadedCachedFileData,
  actorData: ActorData,
  followerUrls: UrlString[],
  post: string,
) {
  const actorUrl = getActorUrl(root).href;
  const outboxUrl = getOutboxUrl(root).href;
  const postUrl = outboxUrl + v4();
  const now = new Date(Date.now()).toISOString();
  const postJson = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: postUrl,
    type: "Create",
    actor: actorUrl,

    object: {
      id: postUrl + "#post",
      type: "Note",
      published: now,
      attributedTo: actorUrl,
      content: post,
      to: "https://www.w3.org/ns/activitystreams#Public",
      cc: followerUrls,
    },
  };

  await overwriteFile(postUrl, new Blob([JSON.stringify(postJson, null, 4)]), {
    fetch: fetch,
    contentType: "application/ld+json",
  });

  const shares = (
    await Promise.all(
      followerUrls.map(
        async (followerUrl) =>
          [
            followerUrl,
            await sendToFollower(
              privateKey,
              actorData,
              JSON.stringify(postJson),
              followerUrl,
            ),
          ] as const,
      ),
    )
  )
    .filter(([_followerUrl, successfullyShared]) => successfullyShared)
    .map(([followerUrl]) => followerUrl);

  return shares;
}

async function sendToFollower(
  privateKey: LoadedCachedFileData,
  actorData: ActorData,
  post: string,
  followerUrl: UrlString,
) {
  const actor = await fetchActor(followerUrl);
  if (actor === null) {
    return false;
  }

  const parsedActor = parseActor(actor);
  if (parsedActor === null) {
    return false;
  }

  try {
    const response = await postSigned(
      privateKey,
      actorData,
      parsedActor.inboxUrl,
      post,
    );
    return response.ok;
  } catch (e) {
    return false;
  }
}
