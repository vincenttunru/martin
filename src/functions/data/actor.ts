import {
  SolidDataset,
  UrlString,
  WithResourceInfo,
  getSourceUrl,
  getStringNoLocale,
  getThing,
  getUrl,
} from "@inrupt/solid-client";

export type ActorData = {
  handle: string;
  username: string;
  publicKeyUrl: UrlString;
};

export const getActorData = (
  actorDataset: SolidDataset & WithResourceInfo,
): ActorData => {
  const actorResourceUrl = getSourceUrl(actorDataset);
  const hostname = new URL(actorResourceUrl).hostname;
  const actorThing = getThing(actorDataset, actorResourceUrl);

  if (actorThing === null) {
    throw new Error("Your Fediverse profile is malformed.");
  }

  const username = getStringNoLocale(
    actorThing,
    "https://www.w3.org/ns/activitystreams#preferredUsername",
  );
  const handle = `@${username}@${hostname}`;

  const publicKeyUrl = getUrl(
    actorThing,
    "https://w3id.org/security#publicKey",
  );

  if (publicKeyUrl === null || username === null) {
    throw new Error("Your username could not be detected.");
  }

  return {
    username: username,
    handle: handle,
    publicKeyUrl: publicKeyUrl,
  };
};
