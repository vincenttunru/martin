import {
  SolidDataset,
  ThingPersisted,
  UrlString,
  WithResourceInfo,
  getThingAll,
  getUrl,
} from "@inrupt/solid-client";
import { rdf } from "rdf-namespaces";
import { isNotNull } from "../isNotNull";

export const getFollowerList = (
  followerIndexDataset: SolidDataset & WithResourceInfo,
): UrlString[] => {
  const follows = getFollows(followerIndexDataset);
  const followerUrlList = follows
    .map((follow) =>
      getUrl(follow, "https://www.w3.org/ns/activitystreams#actor"),
    )
    .filter(isNotNull);
  return followerUrlList;
};

export function getFollows(dataset: SolidDataset): ThingPersisted[] {
  const follows: ThingPersisted[] = getThingAll(dataset).filter(
    (thing) =>
      getUrl(thing, rdf.type) ===
      "https://www.w3.org/ns/activitystreams#Follow",
  );
  return follows;
}
