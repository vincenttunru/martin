# What is this?

Martin is a proof-of-concept exploring the viability of making a [Solid](https://solidproject.org/) Pod be part of the Fediverse.

# Why?

Well, the federated nature of the Fediverse is laudable, but generally, your data is still on other people's servers. You _can_ host it yourself, but that requires running a full-blown ActivityPub-compliant server, used only for that specific purpose.

The goal here is to be able to use your existing Solid Pod, which also hosts other data, and which can be access through a variety of apps. This could help resolve two of the larger pain points of the current Fediverse:

- having to create separate profiles for separate types of apps (e.g. a Mastodon and a Pixelfed profile), and
- not being able to move your data when you move instances.

# So did it work?

Yes! With some caveats:

- Your Solid Pod does need to be hosted on its own origin, i.e. have its own domain or subdomain. This is because your profile needs to be discoverable through `/.well-known/webfinger`, so Martin needs to be able to set that up for your account specifically.
- Although ActivityPub, the main Fediverse standard, ostensibly is built on Linked Data, in practice, that Linked Data does need to adhere to a particular JSON-LD serialisation. Fediverse software doesn't typically send an `Accept` header, so we can't just pass RDF to the Solid server and leave it up to the server to serialise.
- Martin needs a server-side proxy. This proxy never has access to your Solid Pod and cannot interact with the Fediverse in your name by itself, but it is needed. The reason is that when Martin attempts to notify Fediverse servers of e.g. an accepted follow request, or a new post, those servers don't have the requisite Access-Control-Allow-Origin headers telling your browser that it's OK with requests coming in from another domain, and thus your browser will refuse to send them. Instead, the browser will send the (client-side) signed request to the proxy, which will then forward it to the other server.
- This prototype only allows you to post to the Fediverse. While theoretically, following others and reading their posts should also be possible, that is quite a lot of data to process, and doing that all client-side is likely to run into a memory bottleneck. Thus, if you want to use your Solid Pod for that as well, you are likely going to want to run your own server to act as your agent and keep an eye on your Pod and its ActivityPub inbox.

And one more note: this was really hard! To work with real implementations, you want to be able to see what those implementations (e.g. Mastodon) actually do, but since Solid apps do not run on the Pod server, I wasn't able to inspect incoming requests. Request signing, in particular, to a lot of effort to figure out.

# Future work

For this prototype, I kept all Martin's data in a single Container (`martin_beta`),
except for the Webfinger doc which had to be under `/.well-known/`. This was
useful for debugging and ease of implementation, but it would be interesting to
use Resources already in use by other Solid apps. For example, it might be
possible to re-use the WebID profile document for the actor profile.

Although there's no reason it shouldn't work with other servers, I only tested this with the NSS instance on https://solidcommunity.net. Unfortunately, at the time of implementaton, solid-client had a bug in its universal access APIs, so I could only support WAC, which is not supported by a public ESS instance. That is now resolved, but I have not been able to test it against ESS, as I did not have access to a public instance where Pods run on their own origin - Inrupt's instance has `storage.inrupt.com` as the root. THe same, unfortunately, holds for CSS. This means that Martin is unable to create a Webfinger document at the root, which is a Mastodon requirement.

Additionally, I only verified that my posts showed up on Mastodon, the most widely-used Fediverse software. While theoretically, Martin adheres to the standards and should work with other software as well, the devil is always in the details.

And finally, of course there is lots of functionality that I would have liked to include, such as changing your avatar, your display name, viewing your followers, following other people, replying to other people, adding hash tags, adding media to your posts, etc.

# How to run

You can try Martin yourself at https://martin.vincenttunru.com.

To run the code locally:

- clone this repository,
- make sure you have the requisite versions of Node and npm installed (tip: [Volta](https://volta.sh/) can handle this for you),
- `cd` into the repository directory,
- run `npm install` to install the dependencies, and finally
- run `npm run dev` to start Martin in dev mode.

You should then be able to see Martin by visiting http://localhost:3000.
